<?php
/*
Plugin Name: Generate Product Add
Plugin URI: https://gitlab.com/luism23/template-generate-pdf
Description: Template Generate Pdf
Author: LuisMiguelNarvaez
Version: 1.0.1
Author URI: https://gitlab.com/luism23
License: GPL
 */

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/luism23/template-generate-pdf',
	__FILE__,
	'Generate Product Add'
);
$myUpdateChecker->setAuthentication('glpat-puSVzsHaWSc66qa_rVr1');
$myUpdateChecker->setBranch('master');

define("TGP_PATH",plugin_dir_path(__FILE__));
define("TGP_URL",plugin_dir_url(__FILE__));

function TGP_get_version() {
    $plugin_data = get_plugin_data( __FILE__ );
    $plugin_version = $plugin_data['Version'];
    return $plugin_version;
}
